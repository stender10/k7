import java.util.HashMap;
import java.util.HashSet;

public class Puzzle {

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
      solutions = 0;

      words = args;

      String all = "";

      for (String word : words) {
         all += word;
      }

      letters = "";

      for (int i = 0; i < all.length(); i++) {
         char c = all.charAt(i);
         if (letters.indexOf(c) < 0) letters += c;
      }

      permute(10, letters.length());

      System.out.println("Total solutions: " + solutions);
   }

   private static String letters;
   private static String[] words;
   private static int solutions;

   // Allikas: http://www.kosbie.net/cmu/fall-09/15-110/handouts/recursion/Cryptarithms.java

   public static void permute(int n, int k) {
      permute(n, k, new HashSet<>(), new int[k]);
   }

   public static void permute(int n, int k, HashSet<Integer> set, int[] permutation) {
      if (set.size() == k)
         doPermute(k, permutation);
      else {
         for (int i = 0; i < n; i++)
            if (!set.contains(i)) {
               permutation[set.size()] = i;
               set.add(i);
               permute(n, k, set, permutation);
               set.remove(i);
            }
      }
   }

   public static void doPermute(int k, int[] permutation) {
      HashMap<Character,Integer> charMap = new HashMap<>();

      for (int i = 0; i < k; i++)
         charMap.put(letters.charAt(i), permutation[i]);

      int[] values = new int[words.length];

      for (int j = 0; j < words.length; j++) {
         String word = words[j];

         if (charMap.get(word.charAt(0)) == 0) return;

         int val = 0;

         for (int i = 0; i < word.length(); i++)
            val = 10 *  val + charMap.get(word.charAt(i));

         values[j] = val;
      }

      int additions = 0;
      int sum = values[values.length - 1];

      for (int i = 0; i < values.length - 1; i++) {
         additions += values[i];
      }

      if (additions == sum) {
         solutions++;
         System.out.println(charMap);
      }
   }
}
