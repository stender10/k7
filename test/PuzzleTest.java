import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author jaanus
 */
public class PuzzleTest {

   @Test(timeout = 20000)
   public void test1() {
      Puzzle.main(new String[]{"LLP", "LINEAR", "LOGIC", "PROLOG"}); // 1 solution
      Puzzle.main(new String[]{"YKS", "KAKS", "KOLM"}); // 234 solutions
      Puzzle.main(new String[]{"SEND", "MORE", "MONEY"}); // 1 solution
      Puzzle.main(new String[]{"COMPLEX", "LAPLACE", "CALCULUS"}); // 1 solution
      Puzzle.main(new String[]{"ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC"});  // 2 solutions
      Puzzle.main(new String[]{"MDRD", "HQKR", "OKU", "MUUPW"});  // 6 solutions
      // {A=1, B=2, C=3, D=4, E=5, F=6, G=7, H=8, I=9, J=0},
      // {A=2, B=3, C=5, D=1, E=8, F=4, G=6, H=7, I=9, J=0}
      assertTrue("There are no tests", true);
   }

//   @Test(timeout = 15000)
//   public void test2() {
//      Alphametics a = new Alphametics("AQ + MATJ + FYQAA + QAMJT = GQRAAU");
////      a.solve();
////
////      a = new Alphametics("OEY + ONYYEW + VGHEOV + YYIHIIR = OYOGIOE");
////      a.solve();
////
////      a = new Alphametics("IUSQJ + QJAO + YOOAAS + JUJRQQI = MJSSJRM");
////      a.solve();
////
////      a = new Alphametics("KQ + NUINX + NNAS + NAEPEUP = NSQNQXX");
////      a.solve();
////
////      a = new Alphametics("OA + ACLV + RRVCUI + AICOYY = OLOILVL");
////      a.solve();
////
////      a = new Alphametics("OPPPPPPPPP + OOPPPPPPPP + OOOPPPPPPP + OOOOPPPPPP + OOOOOPPPPP + OOOOOOPPPP + OOOOOOOPPP + OOOOOOOOPP + OOOOOOOOOP = AZERTYUIOP");
////      a.solve();
////
////      a = new Alphametics("AACO + AACT + AACO + AACC + AACC + AAOC = CBAXO");
////      a.solve();
////
////      a = new Alphametics("ACDCD + ACDCC + ACDCE = DCEAP");
////      a.solve();
//
//      System.out.println("hello1");
//      a = new Alphametics("P + OPPPPPPPPP + OOPPPPPPPP + OOOPPPPPPP + OOOOPPPPPP + OOOOOPPPPP + OOOOOOPPPP + OOOOOOOPPP + OOOOOOOOPP + OOOOOOOOOP = AZERTYUIOP");
//      a.solve();
//      System.out.println("Hello2");
//   }

//   @Test(timeout = 15000)
//   public void test3() {
//      CryptArithmetic a = new CryptArithmetic("AQ + MATJ + FYQAA + QAMJT = GQRAAU");
//      a.solve();
//
//      a = new CryptArithmetic("SEND + MORE = MONEY");
//      a.solve();
//      a = new CryptArithmetic("OEY + ONYYEW + VGHEOV + YYIHIIR = OYOGIOE");
//      a.solve();
//
//      a = new CryptArithmetic("IUSQJ + QJAO + YOOAAS + JUJRQQI = MJSSJRM");
//      a.solve();
//
//      a = new CryptArithmetic("KQ + NUINX + NNAS + NAEPEUP = NSQNQXX");
//      a.solve();
//
//      a = new CryptArithmetic("OA + ACLV + RRVCUI + AICOYY = OLOILVL");
//      a.solve();
//
//      a = new CryptArithmetic("OPPPPPPPPP + OOPPPPPPPP + OOOPPPPPPP + OOOOPPPPPP + OOOOOPPPPP + OOOOOOPPPP + OOOOOOOPPP + OOOOOOOOPP + OOOOOOOOOP = AZERTYUIOP");
//      a.solve();
//
//      a = new CryptArithmetic("AACO + AACT + AACO + AACC + AACC + AAOC = CBAXO");
//      a.solve();
//
//      a = new CryptArithmetic("ACDCD + ACDCC + ACDCE = DCEAP");
//      a.solve();
//
//      System.out.println("hello1");
//      a = new CryptArithmetic("P + OPPPPPPPPP + OOPPPPPPPP + OOOPPPPPPP + OOOOPPPPPP + OOOOOPPPPP + OOOOOOPPPP + OOOOOOOPPP + OOOOOOOOPP + OOOOOOOOOP = AZERTYUIOP");
//      a.solve();
//      System.out.println("Hello2");
//   }

//   @Test(timeout = 15000)
//   public void test4() {
//      Crypt a = new Crypt("SEND + MORE = MONEY");
//      a.solve();
//   }
}
